//When user right clicks on highlighted text, provide the option to go to a
//social media account with that username

const reddit_url = "http://www.reddit.com/user/"
const twitter_url = "http://www.twitter.com/"
const insta_url = "http://www.instagram.com/"
const fb_url = "http://www.facebook.com/"

let linkEnd = (url) => (username) => url + username

let linkToReddit = linkEnd(reddit_url);
let linkToTwitter = linkEnd(twitter_url);
let linkToInsta = linkEnd(insta_url);
let linkToFB = linkEnd(fb_url);

let liveLink = (link) => browser.tabs.create({ url: link });

//Make sure menu items were created
function onCreated() {
	if(browser.runtime.lastError) {
		console.log(`Error: ${browser.runtime.lastError}`);
	} else {
		console.log("Menu item created successfully!");
	}
}

//Create menu items
browser.menus.create({
	id: "reddit-menu",
	title: "Go to %s's profile on Reddit",
	contexts: ["selection"]
},onCreated);

browser.menus.create({
	id: "twitter-menu",
	title: "Go to %s's profile on Twitter",
	contexts: ["selection"]
},onCreated);

browser.menus.create({
	id: "insta-menu",
	title: "Go to %s's profile on Instagram",
	contexts: ["selection"]
},onCreated);

browser.menus.create({
	id: "fb-menu",
	title: "Go to %s's profile on Facebook",
	contexts: ["selection"]
},onCreated);


browser.menus.onClicked.addListener(function(info,tab) {
	
	function onCreated(tab) {
		console.log(`Created new tab: ${tab.id}`);
	}

	function onError(error) {
		console.log(`Error: ${error}`);
	}

	switch (info.menuItemId){
		case 'reddit-menu':
			liveLink(linkToReddit(info.selectionText))
				.then(onCreated, onError);
			break;
		case 'twitter-menu':
			liveLink(linkToTwitter(info.selectionText))
				.then(onCreated, onError);
			break;
		case 'insta-menu':
			liveLink(linkToInsta(info.selectionText))
				.then(onCreated, onError);
			break;
		case 'fb-menu':
			liveLink(linkToFB(info.selectionText))
				.then(onCreated, onError);
	}	
});
